const { genSaltSync, hashSync } = require("bcrypt");
const { validationResult } = require("express-validator");
const { param } = require("./user.route");
const { create, get, deleteUser, getUser, updateUser } = require("./user.services");

module.exports = {
    createUser : (req, res) =>  {
        const errors = validationResult(req);
        const body = req.body;
        console.log(req.body);
        if (!errors.isEmpty()) {
            return res.status(400).json({
                success: 0,
                errors: errors.array()
            });
        }
        
        create(body, (err, results) => {
            if (err) {
                return res.status(500).json(
                    {
                        success : 0,
                        message : 'Database connexion error',
                        stack : err
                    }
                )
            }
            return res.status(201).json({
                success :1,
                data : results
            });
        })
    },
    getUsers : (req, res) => {
       get((err, results) => {
            if (err) {
                return res.status(500).json(
                    {
                        success : 0,
                        message : 'Database connexion error',
                        stack : err
                    }
                )
            }
            return res.status(200).json({
                success :1,
                data : results
            });
                
       }) 
    }, 
    getUser : (req, res) => {
        body = req.body;
        const id = req.params.id;
        getUser(id, (err, results) => {
            if (err) {
                return res.status(500).json(
                    {
                        success : 0,
                        message : 'Database connexion error',
                        stack : err
                    }
                )                    
            } 
            return res.status(200).json({
                success :0,
                data : results
            })

        })

    },
    updateUser : (req, res) => {
        body  = req.body;
        const id  = req.params.id;
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            return res.status(400).json({
                success: 0,
                errors: errors.array()
            });
        }
        updateUser(body, id, (err, results) => {
            if (err) {
                return res.status(500).json(
                    {
                        success : 0,
                        message : 'Database connexion error',
                        stack : err
                    }
                )  
            }
            return res.status(200).json({
                success : 1,
                data : results
            });
        })
    },
    deleteUsers : (req, res) => {
        const id  = req.params.id;
        deleteUser(id, (err, results) => {
            if (err) {
                return res.status(500).json(
                    {
                        success : 0,
                        message : 'Database connexion error',
                        stack : err
                    }
                )                    
            } 
            return res.status(200).json(
               {
                success : 1,
                message : 'User delete successfully'
               }
            )
        })
    }
}