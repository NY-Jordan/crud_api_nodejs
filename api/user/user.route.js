const { createUser, getUsers, deleteUsers, getUser, updateUser } = require('./user.controller');
const { createValidation, deleteValidation } = require('./user.validation');


const router = require('express').Router();

router.post('/', createValidation, createUser);
router.post('/delete/:id', deleteUsers);
router.get('/:id', getUser);
router.post('/update/:id',createValidation, updateUser);
router.get('/', getUsers);

module.exports = router;