
const { check } = require('express-validator');
 
exports.createValidation = [
    check('first_name', 'first_name is requied').not().isEmpty(),
    check('last_name', 'last_name mis required').not().isEmpty(),
    check('gender', 'Gender mis required').not().isEmpty(),
    check('gender', 'Gender mis required').isLength({ max : 1 }),
    check('password', 'Password is required').not().isEmpty(),
    check('password', 'Password must be 6 or more characters').isLength({ min: 6 }),
    check('password', 'Phone is required').not().isEmpty(),

];
 
exports.deleteValidation = [
    check('id', 'the id is required').not().isEmpty()
];
