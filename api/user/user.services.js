const connection  = require("../../config/database")

module.exports = {
    create : (data, callback) => {
        connection.query(
            'insert into users(first_name, last_name, gender, password, phone) values(?,?,?,?,?)',
            [
                data.first_name,
                data.last_name,
                data.gender,
                data.password,
                data.phone,
            ], 
            (error, results, fields) => {
                if (error) {
                    callback(error)
                }
                return callback(null, results)
            }
        );
    },
    get : (callback) => {
        connection.query(
            'select * from users', 
            (error, results, fields) => {
                if (error) {
                    callback(error)
                }
                return callback(null, results)
            }
        );
    },
    getUser : (id, callback) => {
        connection.query(
            'select * from users where id  =  ?', 
            [id],
            (error, results, fields) => {
                if (error) {
                    callback(error)
                }
                return callback(null, results)
            }
        );
    },

    updateUser : (data, id, callback) => {
        connection.query(
            'UPDATE users SET first_name=?, last_name=?,gender=?,password=?,phone=? Where id =  ?', 
            [
                data.first_name,
                data.last_name,
                data.gender,
                data.password,
                data.phone,
                id
            ],
            (error, results, fields) => {
                if (error) {
                    callback(error)
                }
                return callback(null, results)
            }
        );
    },
    deleteUser : (id, callback) => {
        connection.query(
            'delete from users where id = ?',
            [id],
            (error, results, fields) => {
                if (error) {
                    callback(error)
                }
                return callback(null, results)
            }
        )
    }
}