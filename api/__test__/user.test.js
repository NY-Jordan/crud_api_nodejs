const app = require('./../../app');

const request  = require('supertest');

describe('User', () => {
  //test for get a all users
  it('return status code 200 if we get all users', async () => {
    const res  = await (await request(app).get('/api/users'));

    expect(res.statusCode).toEqual(200);
  });

 

   //test for get a specific user and is not found
   it('return status code 200 if we can get an specific user and he is not found', async () => {
    const res  = await (await request(app).get('/api/users/1'));
    expect(res.statusCode).toEqual(200);
    expect(res.body.data).toEqual([]);
  });

  //test for insert an  user
  it('return status code 201 if we can create an  user', async () => {
    const res  = await (await request(app).post('/api/users').send({
        id : 0,
        first_name : "test first name",
        last_name : "test last name",
        phone : "670000000",
        gender : "M",
        password : "password test",
    }));
    expect(res.statusCode).toEqual(201);
  });

   //test for get a specific user
   it('return status code 200 if we can get an specific user', async () => {
    const res  = await (await request(app).get('/api/users/1'));
    expect(res.statusCode).toEqual(200);
  });

  //test for insert an  user and missing a data
  it('return status code 400 if we cannot create  an  user when data is missing', async () => {
    const res  = await (await request(app).post('/api/users'));
    expect(res.statusCode).toEqual(400);
  });


   //test for insert an  user
   it('return status code 200 if we can update an  user', async () => {
    const res  = await (await request(app).post('/api/users/update/1').send({
        id : 0,
        first_name : "test first nam",
        last_name : "test last name",
        phone : "670000000",
        gender : "M",
        password : "password test",
    }));
    expect(res.statusCode).toEqual(200);
  });

   //test for insert an  user
   it('return status code 200 if we can delete an  user', async () => {
    const res  = await (await request(app).post('/api/users/delete/1'));
    expect(res.statusCode).toEqual(200);
  });

})
