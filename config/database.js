const dotenv = require('dotenv');

//get config database in env
dotenv.config()
//sql config initialisation
var mysql      = require('mysql');
var connection = mysql.createConnection({
  host     : process.env.HOST,
  user     :  process.env.DB_USER,
  password : process.env.PASSWORD,
  database : process.env.DB_NAME
});
 
module.exports = connection;