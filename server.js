const app = require('./app');

app.listen(process.env.APP_PORT, () => {
    console.log('server up and running on : http://localhost:' + process.env.APP_PORT);
});

