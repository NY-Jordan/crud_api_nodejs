const bodyParser = require('body-parser');
const express = require('express');
const Userrouter  = require('./api/user/user.route');
const cors = require('cors');

require('dotenv').config()

const app = express();
app.use(express.json()) ;
app.use(express.urlencoded({
    extended : true
}));
app.use(bodyParser.json());


app.use(bodyParser.urlencoded({
    extended: true
}));


 
app.use(cors());
 

app.use('/api/users', Userrouter);

module.exports = app;